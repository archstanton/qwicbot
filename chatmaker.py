from emoji import EmojiSet
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)

import logging
import json
import requests

# nodes
ASK_PARAMS = 1
SET_CUSTOM_PARAMS = 2


# noinspection PyUnusedLocal
class ChatMakerHandler(ConversationHandler):
    def __init__(self, base_url):
        super().__init__(
            entry_points=[CommandHandler('makechat', self.start)],
            states={
                ASK_PARAMS: [
                    MessageHandler(Filters.text, self.set_params),
                    CommandHandler('cancel', self.cancel)
                ],

                SET_CUSTOM_PARAMS: [
                    MessageHandler(Filters.text, self.custom_params),
                    CommandHandler('cancel', self.cancel)
                ]
            },
            fallbacks=[CommandHandler('cancel', self.cancel)]
        )

        self.logger = logging.getLogger(__name__)
        self.url = f'{base_url}/livechat/v1.1/user/connect'

    def start(self, bot, update):
        user = update.message.from_user
        self.logger.info(f'User {user} requested new chat dialog')
        return self.ask_params(bot, update)

    def set_params(self, bot, update):
        text = str(update.message.text).lower()
        user = update.message.from_user
        self.logger.info(f'User {user} requested "{text}" chat')

        if text == 'empty':
            result = self.make_chat()

            if result == 200:
                update.message.reply_text(
                    'Done. New empty issue was just created.',
                    reply_markup=ReplyKeyboardRemove()
                )
            else:
                update.message.reply_text(
                    'Damn. We got some errors.',
                    reply_markup=ReplyKeyboardRemove()
                )

            return ConversationHandler.END

        if text == 'random':
            result = self.make_chat({
                'base/realname': 'Кот Матроскин',
                'base/inn': '6699000000',
                'base/kpp': '669901001',
                'base/login': 'kotmatroskin@skbkontur.ru',
                'base/orgname': 'ЗАО "Простоквашино"'
            })

            if result == 200:
                update.message.reply_text(
                    'Done. New random issue was just created',
                    reply_markup=ReplyKeyboardRemove()
                )
            else:
                update.message.reply_text(
                    'Damn. We got some errors.',
                    reply_markup=ReplyKeyboardRemove()
                )

            return ConversationHandler.END

        if text == 'custom':
            return self.ask_custom_params(bot, update)

        update.message.reply_text(
            f'Sorry, I can\'t do that{EmojiSet.FROWNING}'
        )

        return ConversationHandler.END

    @staticmethod
    def ask_params(bot, update):
        reply_keyboard = [['Empty', 'Random', 'Custom']]

        update.message.reply_text(
            'What kind of chat you desire?',
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

        return ASK_PARAMS

    @staticmethod
    def ask_custom_params(bot, update):
        update.message.reply_text(
            'Enter additional parameters in following format:\n\n'
            'key1 - value1\n'
            'key2 - value2\n',
            reply_markup=ReplyKeyboardRemove())

        return SET_CUSTOM_PARAMS

    def custom_params(self, bot, update):
        text = str(update.message.text)
        self.logger.info(f'Custom params are received:{text}')
        lines = text.splitlines()
        params = {}

        try:
            for line in lines:
                key, value = [t.strip() for t in line.split('-')]
                params[key] = value

        except Exception as e:
            self.logger.error(e)
            update.message.reply_text(f'It seems like wrong format')
            return self.ask_custom_params(bot, update)

        result = self.make_chat(params)
        if result == 200:
            update.message.reply_text(
                f'Ok. Catch your new custom issue{EmojiSet.SMILE}',
                reply_markup=ReplyKeyboardRemove())
        else:
            update.message.reply_text(
                'Damn. We got some errors.',
                reply_markup=ReplyKeyboardRemove()
            )

        return ConversationHandler.END

    def make_chat(self, params=None):
        data = {
            'sourceUrl': 'https://wiki.skbkontur.ru',
            'topicName': 'Default'}

        if params is not None:
            if 'topicName' in params:
                data['topicName'] = params.pop('topicName')

            if 'sourceUrl' in params:
                data['sourceUrl'] = params.pop('sourceUrl')

            data['additionalParameters'] = params

        response = requests.post(
            url=self.url,
            data=json.dumps(data),
            verify=False,
            headers={"Content-Type": "application/json"}
        )

        return response.status_code

    def cancel(self, bot, update):
        user = update.message.from_user
        self.logger.info(f'User {user.first_name} canceled the conversation.')
        update.message.reply_text(f'Ok. Let\'s try it next time {EmojiSet.SMIRKING}',
                                  reply_markup=ReplyKeyboardRemove())

        return ConversationHandler.END

"""
if __name__ == '__main__':
    # for debug
    url = 'https://api.dev.kontur/livechat/v1.1/user/connect'

    data = {
        'sourceUrl': 'https://wiki.skbkontur.ru',
        'topicName': 'Default'}

    response = requests.post(
        url=url,
        data=json.dumps(data, ensure_ascii=False),
        verify=False,
        headers={"Content-Type": "application/json"})
    d = response.json()
    code = response.status_code
    print(response)
"""