import logging
import yaml

from telegram.ext import (Updater, CommandHandler)
from chatlinks import ChatLinksHandler
from chatmaker import ChatMakerHandler
from emoji import EmojiSet


commands = '/help - list of commands\n' + \
           '/hello - greeting (be polite)\n' + \
           '/chatlink - search linked chats by incident number\n'

logger = logging.getLogger(__name__)

def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def hello(bot, update):
    update.message.reply_text(f'Hello! I am bot {EmojiSet.SMILE}')


def help(bot, update):
    update.message.reply_text(f'You can control me by sending these commands:\n\n{commands}')


def main():
    config_path = "C:\\Users\\koshheev_na\\Desktop\\Bots\\qwicbot\\config.yml"
    with open(config_path, 'r') as configfile:
        config = yaml.load(configfile)

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO,
                        filename=config['log'],
                        filemode='a')

    livechat_config = config.get('livechat')
    chatlinks_config = config.get('chatlinks')
    
    updater = Updater(config['token'])
    dp = updater.dispatcher

    dp.add_handler(CommandHandler('hello', hello))
    dp.add_handler(CommandHandler('help', help))
    
    dp.add_handler(ChatLinksHandler(
        server=chatlinks_config['server'],
        database=chatlinks_config['database'],
        user=chatlinks_config['user'],
        password=chatlinks_config['password']
    ))

    dp.add_handler(ChatMakerHandler(base_url=livechat_config['baseUrl']))

    dp.add_error_handler(error)

    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
