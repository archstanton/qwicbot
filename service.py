import win32service
import win32serviceutil
import win32api
import win32event
import bot
import threading
import os, sys, string, time


class aservice(win32serviceutil.ServiceFramework):
    _svc_name_ = "QwicBot"
    _svc_display_name_ = "QwicBot"
    _svc_description_ = "Bots"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        import servicemanager
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))

        # self.timeout = 640000    #640 seconds / 10 minutes (value is in milliseconds)
        self.timeout = 120000  # 120 seconds / 2 minutes
        # This is how long the service will wait to run / refresh itself (see script below)

        example = Worker()
        example.start()

        while 1:
            # Wait for service stop signal, if I timeout, loop again
            rc = win32event.WaitForSingleObject(self.hWaitStop, self.timeout)
            # Check to see if self.hWaitStop happened
            if rc == win32event.WAIT_OBJECT_0:
                # Stop signal encountered
                servicemanager.LogInfoMsg("SomeShortNameVersion - STOPPED!")  # For Event Log
                example.join()
                break
            
        def main():
            bot.main()

def ctrlHandler(ctrlType):
    return True

class Worker(threading.Thread):

    def run(self):
        bot.main()

if __name__ == '__main__':
    win32api.SetConsoleCtrlHandler(ctrlHandler, True)
    win32serviceutil.HandleCommandLine(aservice)