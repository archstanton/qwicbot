from emoji import EmojiSet
from telegram import ReplyKeyboardRemove
from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)

import logging
import pyodbc

# nodes
ASK_INCIDENT_NUMBER = 1


# noinspection PyUnusedLocal
class ChatLinksHandler(ConversationHandler):
    def __init__(self, server, database, user, password):
        """
        Can find chats linked to incident.
        """
        super().__init__(
            entry_points=[CommandHandler('chatlink', self.start)],

            states={
                ASK_INCIDENT_NUMBER: [
                    MessageHandler(Filters.text, self.incident),
                    CommandHandler("cancel", self.cancel)
                ]
            },

            fallbacks=[CommandHandler('cancel', self.cancel)]
        )

        self.logger = logging.getLogger(__name__)
        self.database = database
        self.connection_string = (
            r'Driver={SQL Server};'
            rf'Server={server};'
            rf'Database={database};'
            rf'UserId={user}'
            rf'Password={password}'
            r'Trusted_Connection=yes;'
        )

    def start(self, bot, update):
        user = update.message.from_user
        self.logger.info(f'User {user.first_name} requested chat links search.')
        update.message.reply_text(
            'Please, enter the incident number.')

        return ASK_INCIDENT_NUMBER

    def incident(self, bot, update):
        text = update.message.text

        try:
            # tying to parse incident number
            num = int(text)

            # process only numbers in working set
            if len(text) > 8 or len(text) < 7:
                return self.not_found(update)

            data = self.find_chats(num)
            if len(data) == 0:
                return self.not_found(update)

            answer = 'I\'m found some chats:'
            for item in data:
                answer += '\n' + item

            update.message.reply_text(answer)

            return ConversationHandler.END
        except ValueError:
            update.message.reply_text(
                f'Нет, нет, нет, дружок-пирожок{EmojiSet.GRINNING} ' +
                'Номер инцидента состоит из 7-8 цифр. ' +
                'Оставь свои SQL-инъекции на потом.')
            return ConversationHandler.END
        except Exception as e:
            return self.unknown_error(update, e)

    def build_query(self, incident):
        return f"""
        SELECT TOP (1000) [ChatsId]
        FROM [{self.database}].[dbo].[IncidentChatsLink]
        LEFT JOIN Incident on IncidentChatsLink.IncidentId = Incident.Id
        WHERE Incident.Number = '{incident}'
        ORDER BY CreationTime
        """

    def find_chats(self, incident):
        connection = pyodbc.connect(self.connection_string)
        cursor = connection.cursor()
        query = self.build_query(incident)

        cursor.execute(query)
        result = []
        while 1:
            row = cursor.fetchone()
            if not row:
                break

            result.append(row[0])
        connection.close()

        return result

    def unknown_error(self, update, exception):
        self.logger.error(exception)
        text = "Oops... something goes wrong. Try again later or call my master."
        update.message.reply_text(text)

        return ConversationHandler.END

    def not_found(self, update):
        self.logger.warning(f'No data was found for chatlinks query: \'{update.message.text}\'.')
        update.message.reply_text(f"Sorry, but I can't find anything {EmojiSet.CONFUSED}")

        return ConversationHandler.END

    def cancel(self, bot, update):
        user = update.message.from_user
        self.logger.info(f'User {user.first_name} canceled the conversation.')
        update.message.reply_text('Ok, honey! Nevermind.',
                                  reply_markup=ReplyKeyboardRemove())

        return ConversationHandler.END


if __name__ == '__main__':
    import yaml
    
    with open("config.yml", 'r') as configfile:
        config = yaml.load(configfile).get('chatlinks')
   
    handler = ChatLinksHandler(
        server=config["server"],
        database=config["database"],
        user=config["userId"],
        password=config["password"])
    
    print(handler.find_chats('8759750'))
