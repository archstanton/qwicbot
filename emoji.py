class EmojiSet:
    SMILE       = '\U0001F603'
    CONFUSED    = '\U0001F615'
    GRINNING    = '\U0001F600'
    SMIRKING    = '\U0001F60F'
    FROWNING    = '\U0001F641' 